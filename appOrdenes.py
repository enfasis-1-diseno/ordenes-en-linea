from abc import ABC, abstractmethod
from typing import Any, Optional, List

'''
La clase Peticion define la estructura de lo que debe contener una peticion para este caso
'''
class Peticion:
    def __init__(self, ip: str, username: str, password: str, rol: str):
        self.ip = ip
        self.username = username
        self.password = password
        self.rol = rol 
        self.desanada = False

    # Prosesor para desanar, desglosar o desencriptar la peticion    
    def desanar(self,) -> None:
        if self.desanada:
            return None
        self.ip = self.ip.replace('*','')
        self.username = self.username.replace('-','')
        self.password = self.password.replace('-','')
        self.rol = self.rol.replace('$','')
        self.desanada = True


'''
La interfaz IValidador declara los metodos que debe tener un validador especifico 
para construir la cadena de resposabilidad de validadores.
'''
class IValidador(ABC):
    
    @abstractmethod
    def definir_siguiente(self, validador):
        pass

    @abstractmethod
    def validar(self, peticion: Peticion) -> bool:
        pass

'''
La clase ValidadorBase define un comportamiento comun para los validadores 
en cuanto el desglose de la peticion o desaneao de las credenciales 
'''
class ValidadorBase(IValidador):

    siguiente_validador: IValidador = None
    anterior_validador: IValidador = None

    # Retorna un validador para enlazarlo al siguiente
    def definir_siguiente(self, validador: IValidador) -> IValidador:
        self.siguiente_validador = validador
        return validador
    
    # Realiza las validaciones como tal 
    @abstractmethod
    def validar(self, peticion: Peticion) -> bool:
        if self.siguiente_validador:
            peticion.desanar()
            return self.siguiente_validador.validar(peticion)
        return True

'''
Todos los validadores especificos deben desglosar y desanar la peticion 
para poder realizar las validaciones y darle paso al siguiente validador 
construyendo así la cadena de resposabilidad 
'''
class AuthValidador(ValidadorBase):

    def __init__(self, usuarios: List[str]):
        self.usuarios = usuarios

    def validar(self, peticion: Peticion) -> bool:
        peticion.desanar()
        if (peticion.username not in self.usuarios) and (len(peticion.password) >= 4):
            print('Credenciales Invalidas')
            return False
        else:
            return super().validar(peticion)


class AdminValidador(ValidadorBase):

    def validar(self, peticion: Peticion) -> bool:
        peticion.desanar()
        if peticion.rol != 'Admin':
            print('Rol Invalido')
            return False
        else:
            return super().validar(peticion)


class IPValidador(ValidadorBase):

    def __init__(self, ip_restringidas: List[str]):
        self.ip_restringidas = ip_restringidas

    def validar(self, peticion: Peticion) -> bool:
        peticion.desanar()
        if peticion.ip in self.ip_restringidas:
            print('IP invalida')
            return False
        else:
            return super().validar(peticion)


def cliente(validador: IValidador) -> None:
    P1 = Peticion('198.212.2.34','juanes','1234','Admin')
    P2 = Peticion('34.1.56.14','kathe','1234','Student')
    P3 = Peticion('0.0.0.0','sebas','1234','Admin')
    P4 = Peticion('10.2.23.76','jesus','1234','Teacher')
    peticiones = [P1, P2, P3, P4]
    
    for p in peticiones:
        print('\n'+p.username)
        result = validador.validar(p)  
        if result:
            print('Paso todos los Validadores ')


if __name__ == "__main__":
    
    # Crear los validadores y definir la cadena de responsabilidad
    ip = IPValidador(['0.0.0.0','127.0.0.1'])
    auth = AuthValidador(['juanes','sebas'])
    admin = AdminValidador()
    ip.definir_siguiente(auth).definir_siguiente(admin)

    # El cliente solo debe llamar el primer validador 
    # para pasar por toda la cadena y obtener el resultado final

    print('*Cadena: IP > Auth > Admin:*')
    cliente(ip)

    print('\n\n*SubCandena: Auth > Admin:*')
    cliente(auth)
